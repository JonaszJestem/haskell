module Subsets where

kSubsets :: Integer -> [Integer] -> [[Integer]]
kSubsets 0 _ = [[]]
kSubsets _ [] = []
kSubsets n (x:xs) = kSubsets n xs ++ [x : subsets | subsets <- kSubsets (n-1) (x:xs)]