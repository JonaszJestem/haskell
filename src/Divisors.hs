module Divisors where

divisors :: Integer -> [Integer]
divisors n = [divisor | divisor <- [1 .. n - 1], n `rem` divisor == 0]