module PerfectNums where
import Control.Monad.Writer
import Divisors

isPerfect :: Integer -> Bool
isPerfect n = sumOfDivisors n == n
  where
    sumOfDivisors n = sum (divisors n)

perfectBelow :: Integer -> Writer [String] Integer
perfectBelow 1 = writer(1, ["Found perfect: 1"])
perfectBelow n = writer (n - 1, logPerfect n) >>= perfectBelow
  where
    logPerfect n = ["Found perfect: " ++ show n | isPerfect n]
