module ExtendedFib where

extendedFib :: Integer -> Integer
extendedFib 0 = 0
extendedFib 1 = 1
extendedFib n = n + extendedFib (n - 1) + extendedFib (n - 2)

betterFib :: Integer -> Integer
betterFib = helper 0 0 0

helper fst sec counter 0 = sec+counter
helper fst sec counter n = helper (sec+counter) (fst+sec+counter) (counter+1) (n-1)