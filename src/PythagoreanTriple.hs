module PythagoreanTriple where

triple :: Integer -> [(Integer,Integer,Integer)]
triple c =
  map (\x -> (x,x+1,c)) triples
  where
    triples = filter formsTriple [1..(c*c)]
    formsTriple a = (a*a) + ((a+1)*(a+1)) == c*c


triplesBelow :: Integer -> [(Integer,Integer,Integer)]
triplesBelow x = concat foundTriples
  where
    foundTriples = filter (not . null) $ map triple [1 .. x]