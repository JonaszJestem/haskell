module SomeSum where
import Divisors

someSum n = sum (map someNumber [1 .. n])
  where
    someNumber k = fromIntegral (numberOfDivisors k) / fromIntegral (product [1 .. k])
    numberOfDivisors n = length (divisors n)
