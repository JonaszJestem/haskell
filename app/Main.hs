module Main where

import           Control.Monad.Writer
import           Data.List
import           ExtendedFib
import           PerfectNums
import           PythagoreanTriple
import           SomeSum
import           Subsets

main = do
  print $ triplesBelow 100
--  print $ extendedFib 5
--  print $ extendedFib 12
--  print $ extendedFib 20
  print $ betterFib 5
  print $ betterFib 12
  print $ betterFib 20
  print $ kSubsets 2 [1, 2, 3]
  print $ perfectBelow 100
  print $ someSum 1000
