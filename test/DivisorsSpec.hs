module DivisorsSpec where

import Test.Hspec
import Test.QuickCheck
import Control.Exception (evaluate)
import Divisors

spec :: Spec
spec = do
  describe "Divisors" $ do
    it "returns the correct divisors" $ do
      divisors 6 `shouldBe` [1,2,3]
      divisors 17 `shouldBe` [1]
      divisors 25 `shouldBe` [1,5]
