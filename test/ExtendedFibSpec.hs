module ExtendedFibSpec where

import Test.Hspec
import Test.QuickCheck
import Control.Exception (evaluate)
import ExtendedFib

spec :: Spec
spec = do
  describe "ExtendedFib" $ do
    it "returns the same results as naive version" $ do
      betterFib 1 `shouldBe` extendedFib 1
      betterFib 2 `shouldBe` extendedFib 2
      betterFib 4 `shouldBe` extendedFib 4
      betterFib 10 `shouldBe` extendedFib 10
      betterFib 20 `shouldBe` extendedFib 20
